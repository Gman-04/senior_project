<?php
include('connection.php');
include('session.php');

?>

<html lang = "en">
<body>
<div class="A option">
<title>Overall </title>
 <link rel="stylesheet" type ="text/css" href="home.css"/>
<h1> Gamified Phishing Simulation </h1>

<ul>
     <li> <a href= "welcome.php"> <?php echo $organization ?> </a> </li>
     <li> <a class = "current" href = "overall.php"> Overall </a> </li>
     <li> <a href = "department.php"> By Department </a> </li>
     <li> <a href = "query.php"> Query Users </a> </li>
     <li> <a href = "addusers.php"> Add Users </a> </li>
     <li> <a href = "logout.php"> Logout </a> </li>
</ul>
<?php

//Prepare query for each level and store data returned, ranges were predetermined to coorelate to a level
$sql1 = "SELECT COUNT(score) FROM users WHERE organization = '$organization' AND score <=0";
$sql2 = "SELECT COUNT(score) FROM users WHERE organization = '$organization' AND score BETWEEN 1 AND 2";
$sql3 = "SELECT COUNT(score) FROM users WHERE organization = '$organization' AND score BETWEEN 3 AND 4";
$sql4 = "SELECT COUNT(score) FROM users WHERE organization = '$organization' AND score BETWEEN 5 AND 6";
$sql5 = "SELECT COUNT(score) FROM users WHERE organization = '$organization' AND score >= 7";

$result1 = mysqli_query($db, $sql1);
$result2 = mysqli_query($db, $sql2);
$result3 = mysqli_query($db, $sql3);
$result4 = mysqli_query($db, $sql4);
$result5 = mysqli_query($db, $sql5);

$level1 = mysqli_fetch_array($result1);
$l1 = $level1[0];

$level2 = mysqli_fetch_array($result2);
$l2 = $level2[0];

$level3 = mysqli_fetch_array($result3);
$l3 = $level3[0];

$level4 = mysqli_fetch_array($result4);
$l4 = $level4[0];

$level5 = mysqli_fetch_array($result5);
$l5 = $level5[0];

?>

  <head>
    <!-- Google Charts -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart', 'bar']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it

	  //connect to MySQL

      function drawChart() {

        // Create the data table.
        //All connected to database
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Level');
        data.addColumn('number', 'Users');
        data.addRows([
		['Level 1', <?php echo $l1; ?>],
		['Level 2', <?php echo $l2; ?>],
		['Level 3', <?php echo $l3; ?>],
		['Level 4', <?php echo $l4; ?>],
		['Level 5', <?php echo $l5; ?>]
        ]);

        // Set chart options
        var options = {width: 900,
                       height: 700,
		       colors: ['#B30000', '#FF0000', '#FFE066', '#99FF99', '#33CC33']
			};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
}
    </script>
</head>
<br>
  <body>
<h2>Users By Level:</h2>
Since users in the lower levels are bad at recognizing phishing emails, you want to see most of your users in level 4 or level 5
<center><div id="chart_div"">

<br><br><br><br>


  </body>
</html>