<?php
include('connection.php');
include('session.php');

?>

<html lang = "en">
<body>
<div class="A option">
<title>Department </title>
 <link rel="stylesheet" type ="text/css" href="home.css"/>
<h1> Gamified Phishing Simulation </h1>

<ul>
     <li> <a href= "welcome.php"> <?php echo $organization ?> </a> </li>
     <li> <a href = "overall.php"> Overall </a> </li>
     <li> <a class = "current" href = "department.php"> By Department </a> </li>
     <li> <a href = "query.php"> Query Users </a> </li>
     <li> <a href = "addusers.php"> Add Users </a> </li>
     <li> <a href = "logout.php"> Logout </a> </li>
</ul>

<?php
//Prepare queries and get data for every level AND each department... definitely could have looped this for better readability
//Computer Science Levels:
$sql1 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND department = 'Computer Science' AND score <=0";
$sql2 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 1 AND 2 AND department = 'Computer Science'";
$sql3 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 3 AND 4 AND department = 'Computer Science'";
$sql4 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 5 AND 6 AND department = 'Computer Science'";
$sql5 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score >=7 AND department = 'Computer Science'";

$result1 = mysqli_query($db, $sql1);
$result2 = mysqli_query($db, $sql2);
$result3 = mysqli_query($db, $sql3);
$result4 = mysqli_query($db, $sql4);
$result5 = mysqli_query($db, $sql5);

$level1 = mysqli_fetch_array($result1);
$cs1 = $level1[0];
$level2 = mysqli_fetch_array($result2);
$cs2 = $level2[0];
$level3 = mysqli_fetch_array($result3);
$cs3 = $level3[0];
$level4 = mysqli_fetch_array($result4);
$cs4 = $level4[0];
$level5 = mysqli_fetch_array($result5);
$cs5 = $level5[0];

//Math Levels:
$sql1 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND department = 'Math' AND score <=0";
$sql2 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 1 AND 2 AND department = 'Math'";
$sql3 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 3 AND 4 AND department = 'Math'";
$sql4 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 5 AND 6 AND department = 'Math'";
$sql5 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score >=7 AND department = 'Math'";

$result1 = mysqli_query($db, $sql1);
$result2 = mysqli_query($db, $sql2);
$result3 = mysqli_query($db, $sql3);
$result4 = mysqli_query($db, $sql4);
$result5 = mysqli_query($db, $sql5);

$level1 = mysqli_fetch_array($result1);
$math1 = $level1[0];
$level2 = mysqli_fetch_array($result2);
$math2 = $level2[0];
$level3 = mysqli_fetch_array($result3);
$math3 = $level3[0];
$level4 = mysqli_fetch_array($result4);
$math4 = $level4[0];
$level5 = mysqli_fetch_array($result5);
$math5 = $level5[0];

//Nursing Levels:
$sql1 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND department = 'Nursing' AND score <=0";
$sql2 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 1 AND 2 AND department = 'Nursing'";
$sql3 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 3 AND 4 AND department = 'Nursing'";
$sql4 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score BETWEEN 5 AND 6 AND department = 'Nursing'";
$sql5 = "SELECT COUNT(department) FROM users WHERE organization = '$organization' AND score >=7 AND department = 'Nursing'";

$result1 = mysqli_query($db, $sql1);
$result2 = mysqli_query($db, $sql2);
$result3 = mysqli_query($db, $sql3);
$result4 = mysqli_query($db, $sql4);
$result5 = mysqli_query($db, $sql5);

$level1 = mysqli_fetch_array($result1);
$nur1 = $level1[0];
$level2 = mysqli_fetch_array($result2);
$nur2 = $level2[0];
$level3 = mysqli_fetch_array($result3);
$nur3 = $level3[0];
$level4 = mysqli_fetch_array($result4);
$nur4 = $level4[0];
$level5 = mysqli_fetch_array($result5);
$nur5 = $level5[0];

?>

<head>
    <!-- Used Google Charts -->
    <!-- Load the AJAX API -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart', 'bar']});

      // Set a callback to run when the Google Visualization API is loaded.
      
	  google.charts.setOnLoadCallback(drawChart3);
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      
      //since database small, hard coded last two departments so you can get a better idea of visualization once fully implemented
	  function drawChart3() {
      var data3 = google.visualization.arrayToDataTable([
        ['Department', 'Level 1', 'Level 2', 'Level 3', 'Level 4', 'Level 5'],
        ['Computer Science', <?php echo $cs1; ?>, <?php echo $cs2; ?>, <?php echo $cs3; ?>, <?php echo $cs4; ?>, <?php echo $cs5; ?>],
        ['Math', <?php echo $math1; ?>, <?php echo $math2; ?>, <?php echo $math3; ?>, <?php echo $math4; ?>, <?php echo $math5; ?>],
        ['Nursing', <?php echo $nur1; ?>, <?php echo $nur2; ?>, <?php echo $nur3; ?>, <?php echo $nur4; ?>, <?php echo $nur5; ?>],
        ['Physics', 1, 2, 4, 7, 0],
        ['Biology', 2, 3, 6, 8, 9]
      ]);

      var options3 = {
        chartArea: {width: '70%'},
		'width:':800,
		'height':600,
        hAxis: {
          title: 'Total Number of People',
          minValue: 0
        },
        vAxis: {
          title: 'Department'
        },
	bar: {groupWidth: '75%'},
	colors: ['#B30000', '#FF0000', '#FFE066', '#99FF99', '#33CC33']
      };

      var chart3 = new google.visualization.BarChart(document.getElementById('chart3_div'));
      chart3.draw(data3, options3);
    }
    </script>
  </head>
<br>
  <body>
<h2>Department:</h2>
This chart will allow you to see which departments are more susceptible to phishing attacks. We reccomend 
training/holding workshops for the departments with a lot of users in level 1 and level 2.
<br>
<center><div id="chart3_div"">

<br><br><br><br>


  </body>
</html>
