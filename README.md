# Senior Project: Gamified Phishing Simulation
To educate Adelphi�s members of potential phishing attacks, the university routinely sends fake phishing emails to all faculty members. 
The current failure rate for phishing at Adelphi University is 10-20%. 
Instead of sending all users the same email, our system will send personalized emails based on a user's susceptibility to fall for a phishing attack. 
This system will score how users interact with phishing emails which will determine the frequency and the difficulty of the 
emails they receive. By individualizing the type of emails sent, we aim to enhance a user�s knowledge of potential attacks, 
directly reducing the possibility of a successful phishing attack. We expect this proof of concept to be implemented at Adelphi University 
and potentially other organizations in the future.

## Assumptions and Dependencies
With a project like this, we would need to assume that we have a list containing names and email addresses for different organizations. 
We also need to assume that the people we are trying to target are using Google�s emailing service. We will assume that when an admin gives us a list
of users, we create an admin account for them on our website since this cannot currently be done on our system.

## Design
Since our system has a lot of moving parts that all need to work together, we created the following data flow diagram during our first sprint.

![DFD](https://bitbucket.org/Gman-04/senior_project/downloads/DFD.png)

### Database Schema
We needed to store a lot of different information so we created various databases. For security precautions we won't elaborate on the details of every table, however, we did have six different tables.

* User Information
* Template Information
* Tracking Pixel Inforamtion Returned
* Campaing Data to automate
* URL to use as landing page based off of organization
* Administrator Login Information

## User Manual
For this system, there are different ways that a user can interact with the system. There are technically three different "users", an employee, an admin,
and then the person who maintains the entire system to recreate everything. Since our code for everything is included, and we will vaguely go over certain configurations and
programs needed, our user manual will just focus on the employee and the admin.

A user can be an organization's employee and be on the receiving end.
These users are the ones who are sent the emails and scored according to their actions. 

A user could also be an admin. This type of user will provide us with a list of their employees and will be able to login to our website to view the data
collected on their organization. Due to this, we split our user manual into two.
### Programs Needed
* MySQL: for database purposes
* PHP & HTML: to send out emails and create templates, php needed for mail function and to connect to database
* Appache: Currently using appache2 service to host our websites to run php scripts to send out emails and to see the logs for tracking pixels
* Posfix: used for the mailq system on our virtual machine. We used this primarily to see how the mail queue was handeling requests and for debugging purposes  

### Configurations
When dealing with configurations, there are a lot of details with the appache configurations and php configurations on our VM. We will just give a basic overview of general configuration settings:


When installing a SMTP you need to mke sure it is in a state where you have a hostname and destination. You can check the setup from this under /etc/postfix/main.cf after you setup the configuration with postfix. 

The appache configurations are more complex. We have it so that it sends a custom log to another file on our server. We only care about the tracking pixel ID not the whole log so we made this change to the /etc/apache2/sites-enabled/000-default.conf file

* We configured it so that the ServerAdmin was webmaster@localhost and the DocumentRoot was /var/ww/html so that our log file would go into that directory and we can parse all of our data there along with keep it consistent with our other files
 
* There was also an addition to where the custom log was added and an alias for the custom log. This alias was also sent to /var/www/html for keeping everything consistent and in one place. The error log was not changed

* Here are the changes to that log

* ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        Alias "/my/pixel/random.php" "/var/www/html/random.php"
        
When dealing with php configurations we changed it so that we can actually see the issues/errors for the purpose of testing. We needed to make sure that we had libraries installed for php such as mysqli

## Employee User Manual
Employees will have no knowledge of this system, as they would hopefully assume the email they are seeing is a phishing email. The emails a user sees will vary based on the score.
An example of what a user in level 2 would see is below:

![Level2](https://bitbucket.org/Gman-04/senior_project/downloads/Email2.png)

The only interaction an employee will have with our system is through this email. They can click on the link but the landing page they are redirected to is a 
dead end and just displays information on how to respond to phishing emails specific to their organization. Although backend wise a lot will be going on and their actions will be tracked,
as a basic employee user you just need to have an email and (hopefully not) interact with it. An example landing page is below

![LandingPage](https://bitbucket.org/Gman-04/senior_project/downloads/LandingPage.png)

## Admin User Manual
As an admin, you are able to see the information that this system collects. To do you, you first need to navigate to the login.html page. You will see this:

![Login](https://bitbucket.org/Gman-04/senior_project/downloads/Login.png)

Once here, login with the credentials given to you. You cannot create your own admin account from out website so no one can obtain unauthorized access to this information. Upon entering valid credentials, you will then see your homepage.
You can easily see the different options within our system on the navigation bar at the top.


![Home](https://bitbucket.org/Gman-04/senior_project/downloads/Home.png)


The different options currently available are described further upon clicking the links. In the overall tab, you will get an overview of your organization in the form of a pie chart.


![Overall](https://bitbucket.org/Gman-04/senior_project/downloads/Overall.png)


In the Department tab you will see each department and how your users rank in a bar graph. This is beneficial so that way you will know what departments need to be better educated about phishing attacks.


![Department](https://bitbucket.org/Gman-04/senior_project/downloads/Department.png)


In the Query tab, you will be able to search by a level to see the specific users that are in that level. This is beneficial when it comes to identifying what users you would need to further educate.


![Query](https://bitbucket.org/Gman-04/senior_project/downloads/Query.png)


In the Add tab, you will be able to quickly add any new employees instead of feeding us a list to add to the database. Currently we do not have a feature to upload a file or a large list of users. The organization will be defaulted to the organization you are
logged in under, and their default score will of course be zero.


![Add](https://bitbucket.org/Gman-04/senior_project/downloads/Add.png)


Lastly, clicking the Logout tab will end your connection and redirect you back to the login page. We also included an About page accessible before logging in that describes our system.

## Concluding Remarks

There are still a few components of this system that we would like to implement to get our system fully automated. 
These additions were not necessary for our project to work and were out of scope in terms of our sprints, as we wanted to focus on the main functionality. Since we created this system using agile development,
at the end of the semester we realized we still had a lot of room for improvement and a lot of enhancements we could make.

Aside from wanting to test the use of our campaign database, we would like to further expand our user database, allow for a file 
upload or CVS upload to quickly add users, possibly a sign up page for new users/admins, and create more templates for each level.